#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include "linenoise/linenoise.c"
#include "linenoise/linenoise.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "methods.h"

void main(int argc, char *argv[], char **env)
{

    char CWD[MAX_ARGS];
    char *USER;
    char *HOME;
    char *SHELL = "/home/jacques/Desktop/Assignment/eggshell";
    getcwd(CWD, sizeof(CWD));

    char *line;
    char *token = NULL, *args[MAX_ARGS];

    signal(SIGINT, ControlCHandle);

    //setting environment variables
    setenv("HOME", getenv("HOME"), 1);
    setenv("PATH", getenv("PATH"), 1);
    setenv("PROMPT", "eggshell-1.0 >", 1);
    setenv("USER", getenv("LOGNAME"), 1);
    setenv("SHELL", SHELL, 1);
    setenv("CWD", CWD, 1);
    setenv("TERMINAL", ttyname(STDIN_FILENO), 1);
    setenv("EXITCODE", "0", 1);

    signal(SIGINT, ControlCHandle);

    while(1)
    {
        while ((line = linenoise("eggshell-1.0 >")) != NULL)
        {
            if(strlen(line)==0 || isSpaceOnly(line))
                continue;
            performTask(line);
        }
        // Free allocated memory
        linenoiseFree(line);
    }
}