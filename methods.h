#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>

#ifndef ASSIGNMENT_METHODS_H
#define ASSIGNMENT_METHODS_H

/* operation:  Saves all tuples in array to the file                            */
/* preconditions: tuple_array, array of tuples                                  */
/*                current_size, the current_size of the existing tuple array    */

#define MAX_ARGS 1000


int isSpaceOnly(char *line);
void doSource(char *filename);
int opType(char *string);
int opIndex(char *string);
int hasEquals(char *string);
int hasPipe(char *string);
void ControlCHandle(int sig_num);
void performTask(char *line);
void substring(char s[], char sub[], int p, int l);
void doPrintText(char **args, char *filename, int numOfArgs, int append);
void doPrint(char **args, int numOfArgs);
void doReadInput(char *filename, char *command);
void doFork(char **args, int args_amt);
void doForkPrint(char **args, char *filename, int append, int args_amt);
void doAll();
void doAllPrint(char *filename, int append);
void doSourcePrint(char *filename_to_read, char *filename_to_write, int append);
void doSource(char *filename);
void pipeIt(char **commands, int commands_amt);

#endif //ASSIGNMENT_METHODS_H
